/**********************************************************\

  Auto-generated SwordPluginAPI.h

\**********************************************************/

#include <string>
#include <sstream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "SwordPlugin.h"

#ifndef H_SwordPluginAPI
#define H_SwordPluginAPI

class SwordPluginAPI : public FB::JSAPIAuto
{
public:
    ////////////////////////////////////////////////////////////////////////////
    /// @fn SwordPluginAPI::SwordPluginAPI(const SwordPluginPtr& plugin, const FB::BrowserHostPtr host)
    ///
    /// @brief  Constructor for your JSAPI object.
    ///         You should register your methods, properties, and events
    ///         that should be accessible to Javascript from here.
    ///
    /// @see FB::JSAPIAuto::registerMethod
    /// @see FB::JSAPIAuto::registerProperty
    /// @see FB::JSAPIAuto::registerEvent
    ////////////////////////////////////////////////////////////////////////////
    SwordPluginAPI(const SwordPluginPtr& plugin, const FB::BrowserHostPtr& host) :
        m_plugin(plugin), m_host(host)
    {
        registerMethod("echo",      make_method(this, &SwordPluginAPI::echo));
        registerMethod("testEvent", make_method(this, &SwordPluginAPI::testEvent));
        registerMethod("getModules", make_method(this, &SwordPluginAPI::getModules));
        registerMethod("getBooknames", make_method(this, &SwordPluginAPI::getBooknames));
        registerMethod("getVMax", make_method(this, &SwordPluginAPI::getVMax));
        registerMethod("getVerses", make_method(this, &SwordPluginAPI::getVerses));
        registerMethod("getStrong", make_method(this, &SwordPluginAPI::getStrong));

        // Read-write property
        registerProperty("testString",
                         make_property(this,
                                       &SwordPluginAPI::get_testString,
                                       &SwordPluginAPI::set_testString));

        // Read-only property
        registerProperty("version",
                         make_property(this,
                                       &SwordPluginAPI::get_version));
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// @fn SwordPluginAPI::~SwordPluginAPI()
    ///
    /// @brief  Destructor.  Remember that this object will not be released until
    ///         the browser is done with it; this will almost definitely be after
    ///         the plugin is released.
    ///////////////////////////////////////////////////////////////////////////////
    virtual ~SwordPluginAPI() {};

    SwordPluginPtr getPlugin();

    // Read/Write property ${PROPERTY.ident}
    std::string get_testString();
    void set_testString(const std::string& val);

    // Read-only property ${PROPERTY.ident}
    std::string get_version();

    // Method echo
    FB::variant echo(const FB::variant& msg);

    //SWORD
    std::string getModules();
    std::string getBooknames(const std::string& moduleName);
    std::string getVMax(const std::string& key);
    std::string getVerses(const std::string& moduleName, const std::string& key, const std::string& single);
    std::string getStrong(const std::string& moduleName, const std::string& key);

    // Event helpers
    FB_JSAPI_EVENT(test, 0, ());
    FB_JSAPI_EVENT(echo, 2, (const FB::variant&, const int));

    // Method test-event
    void testEvent();

private:
    SwordPluginWeakPtr m_plugin;
    FB::BrowserHostPtr m_host;

    std::string m_testString;
};

#endif // H_SwordPluginAPI

