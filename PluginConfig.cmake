#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for Sword Plugin
#
#\**********************************************************/

set(PLUGIN_NAME "SwordPlugin")
set(PLUGIN_PREFIX "SPL")
set(COMPANY_NAME "zefanjas")

# ActiveX constants:
set(FBTYPELIB_NAME SwordPluginLib)
set(FBTYPELIB_DESC "SwordPlugin 1.0 Type Library")
set(IFBControl_DESC "SwordPlugin Control Interface")
set(FBControl_DESC "SwordPlugin Control Class")
set(IFBComJavascriptObject_DESC "SwordPlugin IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "SwordPlugin ComJavascriptObject Class")
set(IFBComEventSource_DESC "SwordPlugin IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID 132aba59-9517-548b-a3b0-3f2ff4078ba4)
set(IFBControl_GUID 832f2724-7e9e-5bca-b33e-aac8286c099d)
set(FBControl_GUID 0125b10e-9f85-5259-bd40-34b2ebf240bc)
set(IFBComJavascriptObject_GUID 0af36fe6-f2ef-5f67-8d45-f78d4ff78f2f)
set(FBComJavascriptObject_GUID 7cdd7f13-c9b6-53f9-8f49-8a6951f384b9)
set(IFBComEventSource_GUID 5e46d0d3-4d64-5d5f-ab54-499e0b3ddd77)

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "zefanjas.SwordPlugin")
set(MOZILLA_PLUGINID "zefanjas.de/SwordPlugin")

# strings
set(FBSTRING_CompanyName "zefanjas")
set(FBSTRING_FileDescription "Browser plugin powered by the SWORD Project")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2012 zefanjas")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}.dll")
set(FBSTRING_ProductName "Sword Plugin")
set(FBSTRING_FileExtents "")
set(FBSTRING_PluginName "Sword Plugin")
set(FBSTRING_MIMEType "application/x-swordplugin")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 0)
set(FBMAC_USE_COCOA 0)
set(FBMAC_USE_COREGRAPHICS 0)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)
