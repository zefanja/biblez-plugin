/**********************************************************\

  Auto-generated SwordPluginAPI.cpp

\**********************************************************/

#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"
#include "global/config.h"

#include "SwordPluginAPI.h"

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <iterator>

//SWORD HEADERS
#include <swmgr.h>
#include <swmodule.h>
#include <markupfiltmgr.h>
#include <listkey.h>
#include <versekey.h>
#include <swlocale.h>
#include <localemgr.h>
//#include <installmgr.h>
//#include <ftptrans.h>
#include <filemgr.h>

using namespace sword;

SWMgr *displayLibrary = 0;
SWMgr *searchLibrary = 0;

std::string searchModule = "";
std::string searchTerm = "";
std::string searchScope = "";
std::string verseView = "";
std::string remoteSource = "";
std::string modName = "";
int searchType = -2;

///////////////////////////////////////////////////////////////////////////////
/// @fn FB::variant SwordPluginAPI::echo(const FB::variant& msg)
///
/// @brief  Echos whatever is passed from Javascript.
///         Go ahead and change it. See what happens!
///////////////////////////////////////////////////////////////////////////////
FB::variant SwordPluginAPI::echo(const FB::variant& msg)
{
    static int n(0);
    fire_echo("So far, you clicked this many times: ", n++);

    // return "foobar";
    return msg;
}

///////////////////////////////////////////////////////////////////////////////
/// @fn SwordPluginPtr SwordPluginAPI::getPlugin()
///
/// @brief  Gets a reference to the plugin that was passed in when the object
///         was created.  If the plugin has already been released then this
///         will throw a FB::script_error that will be translated into a
///         javascript exception in the page.
///////////////////////////////////////////////////////////////////////////////
SwordPluginPtr SwordPluginAPI::getPlugin()
{
    SwordPluginPtr plugin(m_plugin.lock());
    if (!plugin) {
        throw FB::script_error("The plugin is invalid");
    }
    return plugin;
}

// Read/Write property testString
std::string SwordPluginAPI::get_testString()
{
    return m_testString;
}

void SwordPluginAPI::set_testString(const std::string& val)
{
    m_testString = val;
}

// Read-only property version
std::string SwordPluginAPI::get_version()
{
    return FBSTRING_PLUGIN_VERSION;
}

void SwordPluginAPI::testEvent()
{
    fire_test();
}

//HELPER FUNCTIONS
std::string convertString(std::string s) {
    std::stringstream ss;
    for (size_t i = 0; i < s.length(); ++i) {
        if (unsigned(s[i]) < '\x20' || s[i] == '\\' || s[i] == '"') {
            ss << "\\u" << std::setfill('0') << std::setw(4) << std::hex << unsigned(s[i]);
        } else {
            ss << s[i];
        }
    }
    return ss.str();
}

//BIBLEZ BACKEND API

void refreshManagers() {
    delete displayLibrary;
    delete searchLibrary;
    displayLibrary = new SWMgr(new MarkupFilterMgr(FMT_HTMLHREF));
    searchLibrary = new SWMgr();
    displayLibrary->setGlobalOption("Footnotes","On");
    displayLibrary->setGlobalOption("Headings", "On");
    displayLibrary->setGlobalOption("Strong's Numbers", "On");
    displayLibrary->setGlobalOption("Words of Christ in Red","Off");
}

std::string SwordPluginAPI::getModules() {
    /*Get all installed modules or all modules of a specific type. Set modType to e.g. "Biblical Texts"
    getModules() returns a JSON string */
    refreshManagers();
    std::stringstream modules;
    ModMap::iterator it;

    modules << "[";

    for (it = displayLibrary->Modules.begin(); it != displayLibrary->Modules.end(); it++) {
        SWModule *module = (*it).second;
        if (it != displayLibrary->Modules.begin()) {
            modules << ", ";
        }
        modules << "{\"name\": \"" << module->Name() << "\", ";
        modules << "\"modType\":\"" << module->Type() << "\", ";
        if (module->getConfigEntry("Lang")) {
            modules << "\"lang\": \"" << module->getConfigEntry("Lang") << "\", ";
        }
        modules << "\"dataPath\":\"" << module->getConfigEntry("DataPath") << "\", ";
        modules << "\"descr\": \"" << convertString(module->Description()) << "\"}";
    }
    modules << "]";
    return modules.str();
}

std::string SwordPluginAPI::getBooknames(const std::string& moduleName) {
    std::stringstream bnames;
    std::string bnStr;

    SWModule *module = displayLibrary->getModule(moduleName.c_str());
    /*if (!module) {
        PDL_JSException(parms, "getBooknames: Couldn't find Module");
        return PDL_TRUE;  // assert we found the module
    }

    VerseKey *vkey = dynamic_cast<VerseKey *>(module->getKey());
    if (!vkey) {
        PDL_JSException(parms, "getBooknames: Couldn't find verse!");
        return PDL_TRUE;    // assert our module uses verses
    } */
    VerseKey *vkey = dynamic_cast<VerseKey *>(module->getKey());
    VerseKey &vk = *vkey;

    bnames << "[";
    for (int b = 0; b < 2; b++) {
        vk.setTestament(b+1);
        for (int i = 0; i < vk.BMAX[b]; i++) {
            vk.setBook(i+1);
            bnames << "{\"name\": \"" << convertString(vk.getBookName()) << "\", ";
            bnames << "\"abbrev\": \"" << convertString(vk.getBookAbbrev()) << "\", ";
            bnames << "\"cmax\": \"" << vk.getChapterMax() << "\"}";
            if (i+1 == vk.BMAX[b] && b == 1) {
                bnames << "]";
            } else {
                bnames << ", ";
            }
        }
    }

    return bnames.str();
}

std::string SwordPluginAPI::getVMax(const std::string& key) {
    /*Get max number of verses in a chapter*/
    std::stringstream vmax;

    VerseKey vk(key.c_str());
    vmax << vk.getVerseMax();

    return vmax.str();
}

std::string SwordPluginAPI::getVerses(const std::string& moduleName, const std::string& key, const std::string& single) {
    /*Get verses from a specific module (e.g. "ESV"). Set your biblepassage in key e.g. "James 1:19" */
    std::stringstream passage;
    std::stringstream tmpPassage;
    std::stringstream out;

    SWModule *module = displayLibrary->getModule(moduleName.c_str());
    /* if (!module || !(strcmp(module->Type(), "Biblical Texts") == 0 || strcmp(module->Type(), "Commentaries") == 0)) {
        PDL_JSException(parms, "getVerses: Module isn't verse driven (no bible or commentary). Currently BibleZ HD doesn't support Generic Books and Lexicons / Dictionaries!");
        return PDL_TRUE;
    } */

    //module->setKey(key);

    //VerseKey *vk = (VerseKey*)module->getKey();
    VerseKey *vk = dynamic_cast<VerseKey *>(module->getKey());
    //vk->AutoNormalize(false);
    vk->Headings(true);
    vk->setText(key.c_str());

    ListKey verses = VerseKey().ParseVerseList(key.c_str(), "", true);

    passage << "{\"bookName\": \"" << vk->getBookName() << "\", \"cnumber\": \"" << vk->Chapter()  << "\", \"vnumber\": \"" << vk->Verse() << "\", \"passageSingle\" : \"" << vk->getBookName() << " " << vk->Chapter() << ":" << vk->Verse() << "\", \"passage\" : \"" << vk->getBookName() << " " << vk->Chapter() << "\", \"abbrev\": \"" << vk->getBookAbbrev() << "\"}";
    if (single == "true") {
        verses = VerseKey().ParseVerseList(key.c_str(), "", true);
    } else {
        tmpPassage << vk->getBookName() << " " << vk->Chapter();
        verses = VerseKey().ParseVerseList(tmpPassage.str().c_str(), "", true);
    }

    AttributeTypeList::iterator i1;
    AttributeList::iterator i2;
    AttributeValue::iterator i3;

    out << "[";

    for (verses = TOP; !verses.Error(); verses++) {
        vk->setText(verses);

        if (strcmp(module->RenderText(), "") != 0) {
            //headingOn = 0;
            out << "{\"content\": \"" << convertString(module->RenderText()) << "\", ";
            out << "\"vnumber\": \"" << vk->Verse() << "\", ";
            out << "\"cnumber\": \"" << vk->Chapter() << "\"";
            out << ", \"heading\": \"" << convertString(module->getEntryAttributes()["Heading"]["Preverse"]["0"].c_str()) << "\"";

            for (i1 = module->getEntryAttributes().begin(); i1 != module->getEntryAttributes().end(); i1++) {
                if (strcmp(i1->first, "Footnote") == 0) {
                    out << ", \"footnotes\": [";
                    for (i2 = i1->second.begin(); i2 != i1->second.end(); i2++) {
                        out << "{";
                        for (i3 = i2->second.begin(); i3 != i2->second.end(); i3++) {
                            out << "\"" << i3->first << "\": \"" << convertString(i3->second.c_str()) << "\"";
                            //footnotesOn = 1;
                            if (i3 != --i2->second.end()) {
                                out << ", ";
                            }
                        }
                        out << "}";
                        if (i2 != --i1->second.end()) {
                            out << ", ";
                        }
                    }
                    out << "]";
                } /*else if (strcmp(i1->first, "Word") == 0) {
                    out << ", \"words\": [";
                    for (i2 = i1->second.begin(); i2 != i1->second.end(); i2++) {
                        out << "{";
                        for (i3 = i2->second.begin(); i3 != i2->second.end(); i3++) {
                            out << "\"" << i3->first << "\": \"" << convertString(i3->second.c_str()) << "\"";
                            if (i3 != --i2->second.end()) {
                                out << ", ";
                            }
                        }
                        out << "}";
                        if (i2 != --i1->second.end()) {
                            out << ", ";
                        }
                    }
                    out << "]";
                } */
            }

            if (vk->Chapter() == 1 && vk->Verse() == 1) {
                vk->setChapter(0);
                vk->setVerse(0);
                out << ", \"intro\": \"" << convertString(module->RenderText()) << "\"";
            }

            out << "}";

            ListKey helper = verses;
            helper++;
            if (!helper.Error()) {
                out << ", ";
            }
        }
    }

    out << "]";

    /*if (out.str() == "[]") {
        PDL_JSException(parms, "getVerses: Chapter is not available in this module!");
        return PDL_FALSE;
    }*/

    out << "<#split#>" << passage.str();

    return out.str();
}

std::string SwordPluginAPI::getStrong(const std::string& moduleName, const std::string& key) {
    std::stringstream out;

    SWModule *module = displayLibrary->getModule(moduleName.c_str());
    /* if (!module) {
        PDL_JSException(parms, "getStrong: You have to install the 'StrongsGreek' and 'StrongsHebrew' module to get Strong's Numbers!");
        return PDL_TRUE;
    } */

    module->setKey(key.c_str());

    if (strcmp(module->RenderText(), "") != 0) {
        out << convertString(module->RenderText());
    }

    return out.str();
}